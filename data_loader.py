import pandas as pd
from sklearn.metrics import accuracy_score
import joblib
import numpy as np
from pathlib import Path
from optuna.integration.lightgbm import lgb

csvs = Path(".").rglob("*.csv")
GAME_COLS = [
    "GAME_DATE_EST",
    "GAME_ID",
    "HOME_TEAM_ID",
    "VISITOR_TEAM_ID",
    "SEASON",
    "HOME_TEAM_WINS",
]

for csv in csvs:
    if str(csv) == "games.csv":
        df = pd.read_csv(
            csv,
            usecols=GAME_COLS,
            parse_dates=["GAME_DATE_EST"],
            infer_datetime_format=True,
        )
        df = (
            df.drop_duplicates()
            .sort_values("GAME_DATE_EST")
            .set_index(["GAME_DATE_EST"])
        )
    if str(csv) == "ranking.csv":
        df_ranking = pd.read_csv(csv, parse_dates=["STANDINGSDATE"])
        df_ranking.sort_values("STANDINGSDATE", inplace=True)
        df_ranking.drop(
            ["CONFERENCE", "LEAGUE_ID", "HOME_RECORD", "ROAD_RECORD"],
            axis=1,
            inplace=True,
        )

        df_ranking.set_index("STANDINGSDATE", inplace=True)


df = pd.merge_asof(
    df,
    df_ranking.add_suffix("_homeTeam"),
    left_index=True,
    right_index=True,
    left_by="HOME_TEAM_ID",
    right_by="TEAM_ID" + "_homeTeam",
    allow_exact_matches=False,
)
df = pd.merge_asof(
    df,
    df_ranking.add_suffix("_awayTeam"),
    left_index=True,
    right_index=True,
    left_by="VISITOR_TEAM_ID",
    right_by="TEAM_ID" + "_awayTeam",
    allow_exact_matches=False,
)

df.drop(
    ["SEASON_ID_awayTeam", "TEAM_ID_awayTeam", "TEAM_ID_homeTeam"],
    axis=1,
    inplace=True,
)  ## redundant
df.rename(columns={"SEASON_ID_homeTeam": "SEASON_ID"}, inplace=True)
print(df.shape)


def missing_game_rankings(row, suffix="_homeTeam"):
    if (row["G" + suffix] == 0) & (row["W" + suffix] == 0):
        row["G" + suffix] = np.nan
        row["W" + suffix] = np.nan
        row["L" + suffix] = np.nan
        row["W_PCT" + suffix] = np.nan
    return row


df = df.apply(lambda x: missing_game_rankings(x, suffix="_awayTeam"), axis=1)
df = df.apply(lambda x: missing_game_rankings(x, suffix="_homeTeam"), axis=1)

print(df.isna().sum())

df = df.dropna()
print("df without nans size:", df.shape[0])
CUTOFF_ROW = int(df.shape[0] * 0.8)
X = df.drop(["SEASON"], axis=1)._get_numeric_data().copy()
X_train = X[:CUTOFF_ROW].drop(["HOME_TEAM_WINS"], axis=1)
print("X_train", X_train.shape)
X_test = X[CUTOFF_ROW:].drop(["HOME_TEAM_WINS"], axis=1)
print("X_test", X_test.shape)
y_train = X[:CUTOFF_ROW]["HOME_TEAM_WINS"]
print("y_train", len(y_train))
y_test = X[CUTOFF_ROW:]["HOME_TEAM_WINS"]
