# nba predictor (binary classification)
## prerequisite
`pip install -r requirements.txt`

## xgboost
after 1000 iteration got params: 
got 64.49% accuracy_score 

```python
FrozenTrial(number=707, value=0.6476063829787234, datetime_start=datetime.datetime(2020, 8, 9, 1, 35, 50, 827949), datetime_complete=datetime.datetime(2020, 8, 9, 1, 35, 51, 284971), params={'booster': 'dart', 'lambda': 6.3208921791932715, 'alpha': 0.043802583590332865, 'max_depth': 4, 'eta': 0.25909053307692637, 'gamma': 0.9967808656656159, 'grow_policy': 'depthwise', 'sample_type': 'uniform', 'normalize_type': 'forest', 'rate_drop': 0.0008559912696509043, 'skip_drop': 0.41026620256641033}, distributions={'booster': CategoricalDistribution(choices=('gbtree', 'gblinear', 'dart')), 'lambda': LogUniformDistribution(high=10.0, low=1e-08), 'alpha': LogUniformDistribution(high=10.0, low=1e-08), 'max_depth': IntUniformDistribution(high=9, low=1, step=1), 'eta': LogUniformDistribution(high=1.0, low=1e-08), 'gamma': LogUniformDistribution(high=1.0, low=1e-08), 'grow_policy': CategoricalDistribution(choices=('depthwise', 'lossguide')), 'sample_type': CategoricalDistribution(choices=('uniform', 'weighted')), 'normalize_type': CategoricalDistribution(choices=('tree', 'forest')), 'rate_drop': LogUniformDistribution(high=1.0, low=1e-08), 'skip_drop': LogUniformDistribution(high=1.0, low=1e-08)}, user_attrs={}, system_attrs={}, intermediate_values={}, trial_id=707, state=TrialState.COMPLETE)
```
to reproduce / consume just use the dumped model `xgb_optimized.pkl`
or train it yourself on `test_xgb.py`


## lgbm
after 100 trial got:    
got 63.833% accuracy_score

to reproduce / consume just use the dumped model `lgbm_optimized.pkl`
or train it yourself on `test_gbm.py`

##  usage
```python 
import joblib

model = joblib.load('./model_name.pkl')

pred = model.predict(X_train) # you need to follow the columns of X_train in data_loader
``` 

